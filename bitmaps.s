/*
 * Copyright (c) 2019 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * Licensed under the MIT license; see LICENSE.txt for details.
 */

* vim: ft=asm68k:

.section .rodata

.global squares_bitmap
.global squares_pal

squares_bitmap:
    .incbin "squares.bitmap"

squares_pal:
    .incbin "squares.pal"
