/*
 * Copyright (c) 2019 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * Licensed under the MIT license; see LICENSE.txt for details.
 */

* vim: ft=asm68k:

.section .vectors

* $000-$0FF - Vectors
    * $00 - Stack pointer
    dc.l 0x0
    * $04 - Code start
    dc.l _entry_point
    * $08 - Bus error
    dc.l 0x0
    * $0C - Address error
    dc.l 0x0
    * $10 - Illegal instruction
    dc.l 0x0
    * $14 - Divistion by zero
    dc.l 0x0
    * $18 - CHK exception
    dc.l 0x0
    * $1C - TRAPV exception
    dc.l 0x0
    * $20 - Privilege violation
    dc.l 0x0
    * $24 - TRACE exeption
    dc.l 0x0
    * $28 - LINE 1010 EMULATOR
    dc.l 0x0
    * $2C - LINE 1111 EMULATOR
    dc.l 0x0
    * $30 - Reserved by Motorola
    dc.l 0x0
    * $34 - Reserved by Motorola
    dc.l 0x0
    * $38 - Reserved by Motorola
    dc.l 0x0
    * $3C - Reserved by Motorola
    dc.l 0x0
    * $40 - Reserved by Motorola
    dc.l 0x0
    * $44 - Reserved by Motorola
    dc.l 0x0
    * $48 - Reserved by Motorola
    dc.l 0x0
    * $4C - Reserved by Motorola
    dc.l 0x0
    * $50 - Reserved by Motorola
    dc.l 0x0
    * $54 - Reserved by Motorola
    dc.l 0x0
    * $58 - Reserved by Motorola
    dc.l 0x0
    * $5C - Reserved by Motorola
    dc.l 0x0
    * $60 - Spurious exception
    dc.l 0x0
    * $64 - Interrupt request level 1
    dc.l 0x0
    * $68 - Interrupt request level 2
    dc.l 0x0
    * $6C - Interrupt request level 3
    dc.l 0x0
    * $70 - Interrupt request level 4 (VDP interrupt / Horizontal blank)
    dc.l 0x0
    * $74 - Interrupt request level 5
    dc.l 0x0
    * $78 - Interrupt request level 6 (Vertical blank)
    dc.l vblank_interrupt
    * $7C - Interrupt request level 7
    dc.l 0x0
    * $80 - TRAP #00 exception
    dc.l 0x0
    * $84 - TRAP #01 exception
    dc.l 0x0
    * $88 - TRAP #02 exception
    dc.l 0x0
    * $8C - TRAP #03 exception
    dc.l 0x0
    * $90 - TRAP #04 exception
    dc.l 0x0
    * $94 - TRAP #05 exception
    dc.l 0x0
    * $98 - TRAP #06 exception
    dc.l 0x0
    * $9C - TRAP #07 exception
    dc.l 0x0
    * $A0 - TRAP #08 exception
    dc.l 0x0
    * $A4 - TRAP #09 exception
    dc.l 0x0
    * $A8 - TRAP #10 exception
    dc.l 0x0
    * $AC - TRAP #11 exception
    dc.l 0x0
    * $B0 - TRAP #12 exception
    dc.l 0x0
    * $B4 - TRAP #13 exception
    dc.l 0x0
    * $B8 - TRAP #14 exception
    dc.l 0x0
    * $BC - TRAP #15 exception
    dc.l 0x0
    * $C0 - Reserved by Motorola
    dc.l 0x0
    * $C4 - Reserved by Motorola
    dc.l 0x0
    * $C8 - Reserved by Motorola
    dc.l 0x0
    * $CC - Reserved by Motorola
    dc.l 0x0
    * $D0 - Reserved by Motorola
    dc.l 0x0
    * $D4 - Reserved by Motorola
    dc.l 0x0
    * $D8 - Reserved by Motorola
    dc.l 0x0
    * $DC - Reserved by Motorola
    dc.l 0x0
    * $E0 - Reserved by Motorola
    dc.l 0x0
    * $E4 - Reserved by Motorola
    dc.l 0x0
    * $E8 - Reserved by Motorola
    dc.l 0x0
    * $EC - Reserved by Motorola
    dc.l 0x0
    * $F0 - Reserved by Motorola
    dc.l 0x0
    * $F4 - Reserved by Motorola
    dc.l 0x0
    * $F8 - Reserved by Motorola
    dc.l 0x0
    * $FC - Reserved by Motorola
    dc.l 0x0

.section .text

_entry_point:
    * enable interrupts
    move.w %sr, %d0
    andi.w #0xf0ff, %d0
    * ignore interrupts <=5, ie. allow 6 and 7
    ori.w  #0x0500, %d0
    move.w %d0, %sr
    bra start
