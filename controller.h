/*
 * Copyright (c) 2019 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * Licensed under the MIT license; see LICENSE.txt for details.
 */

#ifndef SEGA_CONTROLLER_H
#define SEGA_CONTROLLER_H

#include <stdint.h>

class ControlPad {
private:
    uint8_t byte1=0, byte2=0;
public:
    bool up()    const { return !(byte1&1); }
    bool down()  const { return !((byte1>>1)&1); }
    bool left()  const { return !((byte1>>2)&1); }
    bool right() const { return !((byte1>>3)&1); }
    // sic
    bool btnB()  const { return !((byte1>>4)&1); }
    bool btnC()  const { return !((byte1>>5)&1); }
    bool btnA()  const { return !((byte2>>4)&1); }
    bool start() const { return !((byte2>>5)&1); }

    void update();
};

inline void ControlPad::update() {
    *(volatile uint8_t*)0xA10009 = 0b01000000;
    *(volatile uint8_t*)0xA10003 = 0b01000000;
    asm volatile("nop; nop");
    byte1 = *(volatile uint8_t*)0xA10003;
    *(volatile uint8_t*)0xA10003 = 0b00000000;
    asm volatile("nop; nop");
    byte2 = *(volatile uint8_t*)0xA10003;
}

#endif
