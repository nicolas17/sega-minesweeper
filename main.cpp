/*
 * Copyright (c) 2019 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * Licensed under the MIT license; see LICENSE.txt for details.
 */

#include <stdint.h>
#include <atomic>

#include "vdp.h"
#include "controller.h"

#include "resources.h"

#define color(r, g, b) ((r&0x7) << 1 | (g&0x7) << 5 | (b&0x7) << 9)

extern char _etext, _data, _edata, _sdata;
void main();

extern "C" void start() {
    // copy .data section from ROM to RAM
    char* src = &_etext;
    char* dst = &_data;
    while (dst <= &_edata) {
        *dst++ = *src++;
    }
    main();
}

volatile bool vblank_done = false;
uint8_t squares[10][10] = {{}};
uint8_t curx=0, cury=0;

extern "C" __attribute__((interrupt_handler))
void vblank_interrupt()
{
    //redraw whole grid
    for (int y=0; y<10; ++y) {
        for (int x=0; x<10; ++x) {
            uint8_t square = squares[y][x]*4;
            VDP.put_scrollA_tile(x*2,   y*2,   square+1, 1);
            VDP.put_scrollA_tile(x*2+1, y*2,   square+2, 1);
            VDP.put_scrollA_tile(x*2,   y*2+1, square+3, 1);
            VDP.put_scrollA_tile(x*2+1, y*2+1, square+4, 1);
        }
    }

    VDP.write_vram(0x1000, cury+0x80);
    //                      hsize     vsize   link
    VDP.write_vram(0x1002, (1<<10) | (1<<8) | 0);
    //                      pri       pal       sn
    VDP.write_vram(0x1004, (0<<15) | (1<<13) | (0x31));
    VDP.write_vram(0x1006, curx+0x80);

    vblank_done = true;
}

static void wait_vblank() {
    vblank_done = false;
    while (!vblank_done)
        /*spin*/;
}

void main()
{
    uint16_t colors[] = {
        color(0,0,0), // transparent
        color(0,0,0), // black
        color(7,7,7), // white
        color(7,0,0), // red
        color(7,4,0), // orange
        color(7,7,0), // yellow
    };
    VDP.set_register(VDP_t::Register::MODE_SET_2, 0b01000100); // display on, vblank interrupt off
    VDP.set_register(VDP_t::Register::MODE_SET_4, 0b10000001); // H40 mode

    VDP.copy_to_cram(0x00, colors, 6);
    VDP.copy_to_cram(0x20, Resources::main_pal, 16);
    VDP.copy_to_vram8(0x20, Resources::squares, 32*12*4);
    VDP.copy_to_vram8(0x20*(1+12*4), Resources::cursor, 32*4);

    VDP.set_layerA_nametable(0x2000);
    VDP.set_layerB_nametable(0x4000);
    VDP.set_sprite_table(0x1000);
    VDP.set_hscroll_table(0x4000);
    VDP.set_scroll_size(64, 32);

    for (int y=0; y<10; ++y) {
        for (int x=0; x<10; ++x) {
            squares[y][x] = 10;
        }
    }
    VDP.set_register(VDP_t::Register::MODE_SET_2, 0b01100100); // display on, vblank interrupt on

    ControlPad control_pad;
    ControlPad old_pad;
    while (1) {
        control_pad.update();
        if (curx > 0*16 && !old_pad.left()  && control_pad.left())  { curx-=16; }
        if (curx < 9*16 && !old_pad.right() && control_pad.right()) { curx+=16; }
        if (cury > 0*16 && !old_pad.up()    && control_pad.up())    { cury-=16; }
        if (cury < 9*16 && !old_pad.down()  && control_pad.down())  { cury+=16; }
        old_pad = control_pad;

        std::atomic_signal_fence(std::memory_order_seq_cst);

        wait_vblank();
    }
}

VDP_t VDP;
