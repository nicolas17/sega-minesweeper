#!/usr/bin/env python3

# Copyright (c) 2019 Nicolás Alvarez <nicolas.alvarez@gmail.com>
#
# Licensed under the MIT license; see LICENSE.txt for details.

import sys
import os
import re
import struct
import argparse

from PIL import Image

def chunk(iterable, n):
    sublist = []
    for item in iterable:
        sublist.append(item)
        if len(sublist) == n:
            yield sublist
            sublist = []

    if len(sublist) > 0:
        yield sublist

class Resource:
    def get_type(self): return "uint8_t"
    def fmt(self): return "0x%x"
    def get_data(self):
        raise NotImplementedError()

class Palette(Resource):
    def __init__(self, pal):
        self.pal = pal

    def get_type(self): return "uint16_t"
    def fmt(self): return "0x%03x"

    def get_data(self):
        for color in chunk(self.pal, 3):
            word = (color[0]<<1 | color[1]<<5 | color[2]<<9)
            yield word

class BitmapBase(Resource):
    def __init__(self, path):
        self.img = Image.open(path)

        # PIL gives us 256 colors even when the file has 16 palette entries,
        # so we slice the list to get the first 16
        pal = self.img.getpalette()[0:16*3]
        pal = [round(x/255*7) for x in pal]
        self.pal = pal

    def get_type(self): return "uint8_t"
    def fmt(self): return "0x%02x"

    def get_tile_coords(self):
        """
        Returns an iterable of tile coordinates x,y in the order
        they have to be output.
        """
        raise NotImplementedError()

    def get_data(self):
        for tile_x, tile_y in self.get_tile_coords():
            for y in range(8):
                for x in range(0,8,2):
                    posx = tile_x*8+x
                    posy = tile_y*8+y
                    p1 = self.img.getpixel((posx,   posy))
                    p2 = self.img.getpixel((posx+1, posy))
                    yield (p1 << 4 | p2)

class Bitmap(BitmapBase):
    def get_tile_coords(self):
        for tile_y in range(self.img.height//8):
            for tile_x in range(self.img.width//8):
                yield (tile_x, tile_y)

class Sprite(BitmapBase):
    # Currently the only difference with sprites is the tile order
    def get_tile_coords(self):
        for tile_x in range(self.img.width//8):
            for tile_y in range(self.img.height//8):
                yield (tile_x, tile_y)

class ScriptParser:
    def parse_file(self, f, basepath=''):
        self.resources = {}
        for line in f:
            line = line.rstrip()
            if line == '': continue

            m = re.fullmatch(r'BITMAP\s+(\w+)\s+"([^"]+)"', line)
            if m:
                self.handle_bitmap(m.group(1), os.path.join(basepath, m.group(2)))
                continue

            m = re.fullmatch(r'SPRITE\s+(\w+)\s+"([^"]+)"', line)
            if m:
                self.handle_sprite(m.group(1), os.path.join(basepath, m.group(2)))
                continue

            m = re.fullmatch(r'PALETTE\s+(\w+)\s+FROM\s*\(([^)]+)\)', line)
            if m:
                bitmaps = [name.strip() for name in m.group(2).split(',')]
                self.handle_palette(m.group(1), bitmaps)
                continue

            raise RuntimeError("Don't understand line %r" % line)
        return self.resources

    def handle_bitmap(self, name, path):
        self.resources[name] = Bitmap(path)

    def handle_sprite(self, name, path):
        self.resources[name] = Sprite(path)

    def handle_palette(self, pal_name, bitmap_names):
        if len(bitmap_names) == 0:
            raise RuntimeError("Palette %r references 0 bitmaps" % pal_name)

        # Validate bitmap list
        for bmp_name in bitmap_names:
            if bmp_name not in self.resources:
                raise RuntimeError("Palette %r references unknown bitmap %r" % (pal_name, bmp_name))
            if not isinstance(self.resources[bmp_name], BitmapBase):
                raise RuntimeError("Palette %r references %r which isn't a bitmap" % (pal_name, bmp_name))

        # Validate that all referenced bitmaps have the same palette
        first_bmp = self.resources[bitmap_names[0]]
        for bmp_name in bitmap_names[1:]:
            bmp = self.resources[bmp_name]
            if first_bmp.pal != bmp.pal:
                raise RuntimeError("Palette %r references %r and %r which have different colors" % (pal_name, bitmap_names[0], bmp_name))

        self.resources[pal_name] = Palette(first_bmp.pal)

def write_output(f, resources, header=None):
    if header:
        f.write('#include "%s"\n' % header)
    f.write('namespace Resources {\n\n')
    for name, res in resources.items():
        f.write("const %s %s[] = {\n" % (res.get_type(), name))
        for group in chunk(res.get_data(), 16):
            f.write("  ")
            for byte in group:
                f.write((res.fmt() % byte) + ',')
            f.write("\n")
        f.write("};\n\n")
    f.write('} // namespace Resources\n')

def write_header(f, resources, guard):
    f.write('#ifndef {0}\n#define {0}\n\n'.format(guard));
    f.write('#include <stdint.h>\n\n')
    f.write('namespace Resources {\n\n')

    for name, res in resources.items():
        f.write("extern const %s %s[];\n" % (res.get_type(), name))

    f.write('\n')
    f.write('} // namespace Resources\n')
    f.write('#endif // include guard\n')

def __main__():
    argparser = argparse.ArgumentParser(description='Resource compiler')
    argparser.add_argument('input', help="path to resource definition file")
    argparser.add_argument('output', help="path to the C++ source code file to output")
    argparser.add_argument('--header', help="path to C++ header file to output (optional)")
    argparser.add_argument('--guard', default="RESOURCE_H", help="include-guard macro name to use in the generated .h file")

    args = argparser.parse_args()

    scriptparser = ScriptParser()

    with open(args.input, "r") as fin:
        basepath = os.path.dirname(args.input)
        resources = scriptparser.parse_file(fin, basepath)

    with open(args.output, "w") as fout:
        write_output(fout, resources, args.header)

    if args.header:
        with open(args.header, "w") as fout:
            write_header(fout, resources, args.guard)

if __name__ == '__main__':
    __main__()
