/*
 * Copyright (c) 2019 Nicolás Alvarez <nicolas.alvarez@gmail.com>
 *
 * Licensed under the MIT license; see LICENSE.txt for details.
 */

#ifndef VDP_H
#define VDP_H

#include <stdint.h>

class VDP_t {
public:
    enum class Register: uint8_t {
        MODE_SET_1 = 0,
        MODE_SET_2 = 1,
        NAMETABLE_BASE_SCROLL_A = 2,
        NAMETABLE_BASE_WINDOW = 3,
        NAMETABLE_BASE_SCROLL_B = 4,
        SPRITE_ATTRIB_BASE = 5,
        BGCOLOR = 7,
        H_INTERRUPT = 10,
        MODE_SET_3 = 11,
        MODE_SET_4 = 12,
        H_SCROLL_BASE = 13,
        AUTO_INCREMENT = 15,
        SCROLL_SIZE = 16,
        WINDOW_H_POS = 17,
        WINDOW_V_POS = 18,
        DMA_LENGTH_LOW = 19,
        DMA_LENGTH_HIGH = 20,
        DMA_SOURCE_LOW = 21,
        DMA_SOURCE_MID = 22,
        DMA_SOURCE_HIGH = 23,

        // Debug registers added by the Gens KMod emulator;
        // not available on real hardware
        KMOD_GENS_CONTROL = 29,
        KMOD_MESSAGE_PUTC = 30,
        KMOD_TIMER = 31
    };

    // low-level operations
    void write_control_port(uint16_t x) {
        *(volatile uint16_t*)0xc00004 = x;
    }
    void write_data_port(uint16_t x) {
        *(volatile uint16_t*)0xc00000 = x;
    }
    void write_cram(uint8_t addr, uint16_t data) {
        write_control_port(0xc000 | addr);
        write_control_port(0);
        write_data_port(data);
    }
    void write_vram(uint16_t addr, uint16_t data) {
        write_control_port(0x4000 | (addr & 0x3fff));
        write_control_port(addr >> 14);
        write_data_port(data);
    }
    // size in words
    void copy_to_cram(uint16_t addr, const uint16_t* data, uint16_t size) {
        set_register(Register::AUTO_INCREMENT, 2);
        write_control_port(0xc000 | addr);
        write_control_port(0);
        for (uint16_t i=0; i<size; ++i) {
            write_data_port(data[i]);
        }
    }
    // size in words
    void copy_to_vram(uint16_t addr, const uint16_t* data, uint16_t size) {
        set_register(Register::AUTO_INCREMENT, 2);
        write_control_port(0x4000 | (addr & 0x3fff));
        write_control_port(addr >> 14);
        for (uint16_t i=0; i<size; ++i) {
            write_data_port(data[i]);
        }
    }
    // size in bytes, but make sure it's an even number
    void copy_to_vram8(uint16_t addr, const uint8_t* data, uint16_t size) {
        copy_to_vram(addr, (uint16_t*)data, size/2);
    }
    void set_register(Register reg, uint8_t data) {
        uint8_t regnum = (uint8_t)reg;
        write_control_port(0x8000 | (regnum<<8) | data);
    }

    void kmod_msg_putc(char c) {
        set_register(Register::KMOD_MESSAGE_PUTC, c);
    }

    // higher level ops

    void kmod_msg_puts(const char* str) {
        do {
            kmod_msg_putc(*str);
        } while (*str++ != 0);
    }
    void set_layerA_nametable(uint16_t addr) {
        // must be a multiple of 0x2000; the last 13 bits must be 0
        if ((addr & 0x1fff) == 0) {
            set_register(Register::NAMETABLE_BASE_SCROLL_A, addr >> 10);
            scrollA_addr = addr;
        } else {
            kmod_msg_puts("set_layerA_nametable called with invalid addr");
        }
    }
    void set_layerB_nametable(uint16_t addr) {
        // must be a multiple of 0x2000; the last 13 bits must be 0
        if ((addr & 0x1fff) == 0) {
            set_register(Register::NAMETABLE_BASE_SCROLL_B, addr >> 13);
            scrollB_addr = addr;
        } else {
            kmod_msg_puts("set_layerB_nametable called with invalid addr");
        }
    }
    void set_sprite_table(uint16_t addr) {
        // Last 9 bits (in H32 mode) or 10 bits (in H40 mode) must be 0.
        // For now we'll only check the last 9,
        // ie. multiple of 0x200.
        if ((addr & 0x1ff) == 0) {
            set_register(Register::SPRITE_ATTRIB_BASE, addr >> 9);
        } else {
            kmod_msg_puts("set_sprite_table called with invalid addr");
        }
    }
    void set_hscroll_table(uint16_t addr) {
        // must be a multiple of 0x2000; the last 10 bits must be 0
        if ((addr & 0x3ff) == 0) {
            set_register(Register::H_SCROLL_BASE, addr >> 10);
        } else {
            kmod_msg_puts("set_hscroll_table called with invalid addr");
        }
    }
    void set_scroll_size(uint8_t width, uint8_t height) {
        uint8_t reg_value;
        switch (width) {
        case  32: reg_value = 0b00; scroll_width = 5; break;
        case  64: reg_value = 0b01; scroll_width = 6; break;
        case 128: reg_value = 0b11; scroll_width = 7; break;
        default:
            kmod_msg_puts("set_scroll_size called with invalid size");
            return;
        }
        switch (height) {
        case  32: reg_value |= 0b00 << 4; scroll_height = 5; break;
        case  64: reg_value |= 0b01 << 4; scroll_height = 6; break;
        case 128: reg_value |= 0b11 << 4; scroll_height = 7; break;
        default:
            kmod_msg_puts("set_scroll_size called with invalid size");
            return;
        }
        set_register(Register::SCROLL_SIZE, reg_value);
    }
    void put_scrollA_tile(uint8_t x, uint8_t y, uint16_t tile, uint8_t pal) {
        const uint16_t addr = scrollA_addr + ((y << scroll_width) + x)*2;
        write_vram(addr, tile | (pal << 13));
    }
    uint8_t get_scroll_width() const {
        return 1 << scroll_width;
    }
private:
    // stored as log2 of the actual size
    uint8_t scroll_width=5, scroll_height=5;
    uint16_t scrollA_addr, scrollB_addr;
};

extern VDP_t VDP;

#endif
